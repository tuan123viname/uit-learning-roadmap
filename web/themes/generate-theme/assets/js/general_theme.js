(function($) {
    Drupal.general_theme = Drupal.general_theme || {};
    Drupal.behaviors.actiongeneral_theme = {
        attach: function(context) {
            $(document).ready(function() {
                 Drupal.general_theme.mobileMenu();
            });
            $('.btn-btt').smoothScroll({
             speed: 1000
            });
            $(window).scroll(function() {
                if ($(window).scrollTop() > 300) {
                    $('.btn-btt').show();
                } else {
                    $('.btn-btt').hide();
                }
            }).resize(function() {
                if ($(window).scrollTop() > 200) {
                    $('.btn-btt').show();
                } else {
                    $('.btn-btt').hide();
                }
            });
        }
    };

    Drupal.general_theme.mobileMenu = function() {
        if ($(window).width() <= 991) {
            var overlay = $('<div data-sidebar-right="mask" style="position: fixed; top: 0px; right: 0px; bottom: 0px; left: 0px; z-index: 2999; background-color: rgb(0, 0, 0); opacity: 0.5;"></div>');
            $('.navbar-toggle').once().click(function() {
                if ($(".section-mobile-menu").hasClass("show-menu")) {
                    $(".section-mobile-menu").removeClass("show-menu")
                } else {
                    $(".section-mobile-menu").addClass("show-menu");
                    $('body').append(overlay)
                    overlay.once().click(function() {
                        $(".section-mobile-menu").removeClass("show-menu");
                        overlay.remove();
                    });
                }
            })
            $(".close-menu").once().click(function() {
                $(".section-mobile-menu").removeClass("show-menu");
                overlay.remove()
            });
        }
    }

})(jQuery);