<?php

namespace Drupal\training_program;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Tags;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\Request;

class EntityAutocompleteMatcher extends \Drupal\Core\Entity\EntityAutocompleteMatcher {

  /**
   * Gets matched labels based on a given search string.
   */
  public function getMatches($target_type, $selection_handler, $selection_settings, $string = '') {
//    dump($target_type);
    $request = \Drupal::request();
    $referer = $request->headers->get('referer');
    $base_url = Request::createFromGlobals()->getSchemeAndHttpHost();
    $alias = substr($referer, strlen($base_url));
    $params = Url::fromUri("internal:" . $alias)->getRouteParameters();
    $entity_type = key($params);
//    $node = \Drupal::entityTypeManager()->getStorage($entity_type)->load($params[$entity_type]);

    $matches = [];

    $options = [
      'target_type'      => $target_type,
      'handler'          => $selection_handler,
      'handler_settings' => $selection_settings,
    ];

    $handler = $this->selectionManager->getInstance($options);

    if (isset($string)) {
      // Get an array of matching entities.
      $match_operator = !empty($selection_settings['match_operator']) ? $selection_settings['match_operator'] : 'CONTAINS';
      $entity_labels = $handler->getReferenceableEntities($string, $match_operator, 10);

      // Loop through the entities and convert them into autocomplete output.
      foreach ($entity_labels as $values) {
        foreach ($values as $entity_id => $label) {

          $entity = \Drupal::entityTypeManager()->getStorage($target_type)->load($entity_id);
          $entity = \Drupal::entityManager()->getTranslationFromContext($entity);

          $type = !empty($entity->type->entity) ? $entity->type->entity->label() : $entity->bundle();
          $key = $label . ' (' . $entity_id . ')';
          // Strip things like starting/trailing white spaces, line breaks and tags.
          $key = preg_replace('/\s\s+/', ' ', str_replace("\n", '', trim(Html::decodeEntities(strip_tags($key)))));
          // Names containing commas or quotes must be wrapped in quotes.
          $key = Tags::encode($key);
          $training_program_label = '';
          if ($entity->hasField('field_training_program')) {
            $training_program = $entity->get('field_training_program')->entity;
            $training_program_label = $training_program ? $training_program->label() : '';
          }
          $label = $label . ' (' . $entity_id . ') ['  . $training_program_label .']';
          $matches[] = ['value' => $key, 'label' => $label];
//
        }
      }
    }

    return $matches;
  }

}
