<?php

namespace Drupal\training_program;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\course\Entity\Course;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Drupal\user\Entity\User;

class RecommendManager implements RecommendManagerInterface {

  const CREDIT_SEMESTER_MAX = 25;

  const NN = 20;
  const CN = 16;
  const CSN = 19;
  const KLTN = 22;
  const TT = 21;
  const TC = 17;
  const DC = 18;

  /**
   * @var array
   */
  protected $coursesBySemester;

  /**
   * @var int
   */
  protected $trainingProgramId;

  /**
   * @var EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var Connection
   */
  protected $connection;

  /**
   * RecommendManager constructor.
   * @param EntityTypeManagerInterface $entity_type_manager
   * @param Connection $connection
   */

  /**
   * @var \Drupal\Core\Entity\EntityBase|\Drupal\Core\Entity\EntityInterface|null
   */
  protected $user;

  /**
   * RecommendManager constructor.
   * @param EntityTypeManagerInterface $entity_type_manager
   * @param Connection $connection
   * @param AccountInterface $account
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, Connection $connection, AccountInterface $account) {
    $this->entityTypeManager = $entity_type_manager;
    $this->connection = $connection;
    $this->user = User::load($account->id());
  }

  /**
   * {@inheritdoc}
   */
  public function setTrainingProgramId(int $training_program_id) {
    $this->trainingProgramId = $training_program_id;
  }

  /**
   * @param array $coursesBySemester
   */
  public function setCoursesBySemester(array $coursesBySemester) {
    $this->coursesBySemester = $coursesBySemester;
  }


  /**
   * {@inheritdoc}
   */
  public function getRecommend(array $params) {
    $default_params = [];
    $params = array_merge($params, $default_params);
    $current_level = 6;
    $credit_remove = 12;
    if ($params['certificate'] == 0) {
      $current_level = $params['english'];
      $credit_remove = ($params['english'] - 1) * 4;
    }
    ksort($this->coursesBySemester);
    $this->removeEnglishPassed($current_level);

    $this->removeCoursePassed();
    $credit_remove += $this->countCreditPassed();
    $this->replaceSpecializeCourses($params['specialize']);
    if ($params['orientation'] == 'replace') {
      $this->replaceThesisWithOptionalCourses();
    }
    $success =  $this->arrange(['credit_remove' => $credit_remove, 'semester' => $params['semester']]);

    if ($success) {
      return $this->coursesBySemester;
    }
    else {
      return FALSE;
    }
  }


  /**
   * {@inheritdoc}
   */
  public function getSpecializeCourses(int $course_type_id, int $specialize_id) {
    $query = $this->connection->select('node__field_training_program_course', 'training_program_course');
    $query->condition('training_program_course.entity_id', $this->trainingProgramId);
    $query->join('node__field_specialize', 'specialize', 'specialize.entity_id = training_program_course.field_training_program_course_target_id');
    $query->join('node__field_course_type', 'course_type', 'course_type.entity_id = specialize.entity_id');
    $query->condition('specialize.field_specialize_target_id', $specialize_id)
      ->condition('course_type.field_course_type_target_id', $course_type_id);
    $query->fields('training_program_course', ['field_training_program_course_target_id']);

    $result = $query->execute()->fetchCol();
    return Node::loadMultiple($result);
  }

  /**
   * {@inheritdoc}
   */
  public function removeEnglishPassed($current_level) {
    foreach ($this->coursesBySemester as $semester_key => $semester) {
      foreach ($semester as $key => $item) {
        if ($item->get('field_course_type')->getValue()[0]['target_id'] == self::NN) {
          if ($item->get('field_current')->value < $current_level) {
            unset($this->coursesBySemester[$semester_key][$key]);
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function replaceThesisWithOptionalCourses() {
    $optional_courses = $this->getOptionalCourses();
    foreach ($this->coursesBySemester as $semester_key => $semester) {
      foreach ($semester as $key => $item) {
        if ($item->get('field_course_type')->getValue()[0]['target_id'] == self::KLTN) {
          unset($this->coursesBySemester[$semester_key][$key]);
          foreach ($optional_courses as $optional_course) {
            $this->coursesBySemester[$semester_key][] = $optional_course;
          }
        }
      }
    }
  }


  public function getOptionalCourses() {
    $result = $this->entityTypeManager->getStorage('node')->loadByProperties([
      'type' => 'training_program_course',
      'field_training_program' => $this->trainingProgramId,
      'field_course_type' => self::TT
    ]);
    return $result;
  }
  /**
   * {@inheritdoc}
   */
  public function replaceSpecializeCourses($specialize_id) {
    $specialize_courses = $this->getSpecializeCourses(self::CN, $specialize_id);
    foreach ($this->coursesBySemester as $semester_key => $semester) {
      foreach ($semester as $key => $item) {
        if ($item->get('field_course_type')->getValue()[0]['target_id'] == self::CN) {
          if (!empty($specialize_courses)) {
            $this->coursesBySemester[$semester_key][$key] = array_shift($specialize_courses);
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function arrange($params) {
    $current_semester = $this->getCurrentSemester();
    $total_credit = Node::load($this->trainingProgramId)->get('field_total_credit')->value;
    $this->pushCoursesToNextSemester();
    /** Semester 1 is fixed */
    $credit_semester_1 = $this->countCredit(reset($this->coursesBySemester));
    $total_credit_need = $total_credit - $params['credit_remove'];


    \Drupal::logger("pahon")->error(print_r($current_semester,true));
//      \Drupal::logger("pahon2")->error(print_r($this->coursesBySemester,true));

    if ($current_semester == 1) {
      $credit_semester_average = ($total_credit_need - $credit_semester_1) / ($params['semester'] - 1);
    }
    else {

      $credit_semester_average = ($total_credit_need ) / ($params['semester'] - ($current_semester -1));
        //check everage <14 show error
    }


    if ($credit_semester_average > self::CREDIT_SEMESTER_MAX || $credit_semester_average < 0) {
      return FALSE;
    }
   
    foreach ($this->coursesBySemester as $index => &$semester) {
      if ($index < $current_semester) {
        continue;
      }
      if ($index == $params['semester'] - 1) {
        break;
      }
      $current_credit = $this->countCredit($semester);
      $credit_semester_max = $credit_semester_average + 1;
      if ($credit_semester_max > self::CREDIT_SEMESTER_MAX) {
        $credit_semester_max = self::CREDIT_SEMESTER_MAX;
      }
      $this->fillSemester($index, $current_credit,$credit_semester_max);
    }

    $total_credit_remaining = 0;
    for ($i = $params['semester'] -1; $i <= 8; $i++) {
      $total_credit_remaining += $this->countCredit($this->coursesBySemester[$i]);
    }
    $total_credit_remaining += $this->countCredit($this->coursesBySemester['temp']);
    if ($total_credit_remaining > 25) {
      $this->fillSemester($params['semester'] -1,  $this->countCredit($this->coursesBySemester[$params['semester'] -1]) + 1, self::CREDIT_SEMESTER_MAX);
    }
    else {
      $this->fillSemester($params['semester'] -1, $this->countCredit($this->coursesBySemester[$params['semester'] -1]), $total_credit_remaining * 2/3);
    }

    $this->fillSemester($params['semester'], 0, 25);
    return TRUE;
  }

  /**
   * Get number credit of array training program course
   *
   * @param $training_program_courses
   * @return int
   */
  public function countCredit($training_program_courses) {
    if (!$training_program_courses) {
      return 0;
    }
    $credit = 0;
    foreach ($training_program_courses as $item) {
      $credit += $item->get('field_course')->entity->get('credit')->value;
    }
    return $credit;
  }


  /**
   * @param $current_semester
   * @param $current_credit
   * @param $credit_semester_average
   */
  public function fillSemester($current_semester, $current_credit, $credit_semester_max) {
    $credit_need = $credit_semester_max - $current_credit;

    /**
     * get courses from temp (priority > next semester)
     */
    if (count($this->coursesBySemester['temp']) > 0) {
      foreach ($this->coursesBySemester['temp'] as $key => $item) {
        /** @var NodeInterface $course */
        $course = $item->get('field_course')->entity;
        if ($course->get('credit')->value <= $credit_need) {
          $prerequisite = $this->checkPrerequisite($item, $current_semester);
          if (!$prerequisite)  {
            /** Move course from next semester to current semester */
            array_push($this->coursesBySemester[$current_semester], $item);
            unset($this->coursesBySemester['temp'][$key]);
            $credit_need -= $course->get('credit')->value;

          }
          else {
           $this->swapCoursePrerequisite($key, $prerequisite);
          }

        }
      }
    }
    if (empty($this->coursesBySemester['temp'])) {
      unset($this->coursesBySemester['temp']);
    }
    /**
     * get courses from next semester
     */
    foreach ($this->coursesBySemester[$current_semester + 1] as $key => $item) {
      /** @var NodeInterface $course */
      $course = $item->get('field_course')->entity;
      if ($course->get('credit')->value <= $credit_need && !$this->checkPrerequisite($item, $current_semester)) {
        /** Move course from next semester to current semester */
        array_push($this->coursesBySemester[$current_semester], $item);
        unset($this->coursesBySemester[$current_semester + 1][$key]);
        $credit_need -= $course->get('credit')->value;
      }
    }

    /**
     * chua day thi lay thang nay
     */
    if ($credit_need >= 2) {
      foreach ($this->coursesBySemester[$current_semester + 2] as $key => $item) {
        /** @var NodeInterface $course */
        if ($item) {
          $course = $item->get('field_course')->entity;
          if ($course->get('credit')->value < $credit_need && !$this->checkPrerequisite($item, $current_semester)) {
            /** Move course from next semester to current semester */
            array_push($this->coursesBySemester[$current_semester], $item);
            unset($this->coursesBySemester[$current_semester + 2][$key]);
            $credit_need -= $course->get('credit')->value;
          }
        }
      }
    }
  }

  /**
   * @param NodeInterface $course
   * @return array | bool
   */
  public function checkPrerequisite(NodeInterface $training_program_course, $current_semester) {
    $values = $training_program_course->get('field_prerequisite_of')->getValue();
    $required_ids = [];
    foreach ($values as $item) {
      $required_ids[] = $item['target_id'];
    }
    for ($i = 1; $i <= $current_semester; $i++) {
      foreach ($this->coursesBySemester[$i] as $index => $item) {
        if (in_array($item->id(), $required_ids)) {
          return ['semester' => $i, 'index' => $index];
        }
      }
    }
    return FALSE;
  }

  public function swapCoursePrerequisite($temp_index, $position) {
    $temp = $this->coursesBySemester[$position['semester']][$position['index']];
    $this->coursesBySemester[$position['semester']][$position['index']] = $this->coursesBySemester['temp'][$temp_index];
    $this->coursesBySemester['temp'][$temp_index] = $temp;
  }

  /**
   * @return int
   */
  public function getCurrentSemester() {
    return $this->user->get('field_current_semester')->value ?  $this->user->get('field_current_semester')->value : 1;
  }

  /**
   * @return \Drupal\Core\Entity\EntityInterface[]
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getClassPassed() {
    return $this->entityTypeManager->getStorage('node')->loadByProperties([
      'type' => 'student_class',
      'field_student' => $this->user->id(),
      'field_status' => 'passed'
    ]);
  }

  /**
   * Get Courses passed
   */
  public function getCoursesPassed() {
    $classes_passed = $this->getClassPassed();
    $courses_passed = [];
    foreach ($classes_passed as $class) {
      $courses_passed[] = $class->get('field_class')->entity->get('field_course')->entity;
    }
    return $courses_passed;
  }

  public function countCreditPassed() {
    $count = 0;
    $courses = $this->getCoursesPassed();
    foreach ($courses as $item) {
      $count += $item->get('credit')->value;
    }
    return $count;
  }

  /**
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function removeCoursePassed() {
    $classes_passed = $this->getClassPassed();
    foreach ($this->coursesBySemester as $semester_key => $semester) {
      foreach ($semester as $key => $item) {

        foreach ($classes_passed as $class) {

          if ($item->get('field_course')->getValue()[0]['target_id'] == $class->get('field_class')->entity->get('field_course')->getValue()[0]['target_id']) {

            unset($this->coursesBySemester[$semester_key][$key]);
          }
        }
      }
    }

  }

  public function pushCoursesToNextSemester() {
    $current_semester = $this->getCurrentSemester();
    $this->coursesBySemester['temp'] = [];
    for ($i = 1; $i < $current_semester; $i++) {
      $this->coursesBySemester['temp'] = array_merge($this->coursesBySemester['temp'], $this->coursesBySemester[$i]);
      unset($this->coursesBySemester[$i]);
    }
  }

}
