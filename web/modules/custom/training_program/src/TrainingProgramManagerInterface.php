<?php

namespace Drupal\training_program;

use Drupal\node\NodeInterface;

interface TrainingProgramManagerInterface {

  /**
   * @param $major_id
   * @return mixed
   */
  public function getSpecializesFromMajorId($major_id);

  /**
   * @param NodeInterface $training_program
   * @return mixed
   */
  public function getMajorId(NodeInterface $training_program);

  /**
   * @param $training_program_id
   * @return mixed
   */
  public function getSampleRoadmap($training_program_id);

}
