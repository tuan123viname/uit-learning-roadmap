<?php

namespace Drupal\training_program;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;

class TrainingProgramManager implements TrainingProgramManagerInterface {

  /**
   * @var EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $termStorage;

  /**
   * StudentManager constructor.
   * @param EntityTypeManagerInterface $entity_type_manager
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->termStorage = $entity_type_manager->getStorage('taxonomy_term');
  }

  /**
   * {@inheritdoc}
   */
  public function getSpecializesFromMajorId($major_id) {
    return  $this->termStorage->loadByProperties(['vid' => 'specialize', 'field_major' => $major_id]);
  }

  /**
   * {@inheritdoc}
   */
  public function getMajorId(NodeInterface $training_program) {
    $major_id = NULL;
    $value = $training_program->get('field_major')->getValue();
    if ($value) {
      $major_id = $value[0]['target_id'];
    }
    return $major_id;
  }

  /**
   *{@inheritdoc}
   */
  public function getSampleRoadmap($training_program_id) {

     $result = $this->entityTypeManager->getStorage('node')->loadByProperties([
      'type' => 'sample_roadmap',
      'field_training_program' => $training_program_id
    ]);
     return reset($result);
  }

}
