<?php

namespace Drupal\training_program;

use Drupal\node\NodeInterface;

interface RecommendManagerInterface {

  /**
   * @param int $training_program_id
   * @return mixed
   */
  public function setTrainingProgramId(int $training_program_id);

  /**
   * @param array $courses_by_semester
   * @return mixed
   */
  public function setCoursesBySemester(array $courses_by_semester);

  /**
   * @param array $params[]
   * @return mixed
   */
  public function getRecommend(array $params);

  /**
   * @param $current_level
   */
  public function removeEnglishPassed($current_level);

  /**
   * @param int $course_type_id
   * @param int $specialize_id
   * @return array $specialize_courses[]
   */
  public function getSpecializeCourses(int $course_type_id, int $specialize_id);

  /**
   * @return mixed
   */
  public function replaceThesisWithOptionalCourses();

  /**
   * @param $specialize_id
   */
  public function replaceSpecializeCourses($specialize_id);

  /**
   * @param $params
   * @return mixed
   */
  public function arrange($params);

  /**
   * @return array
   */
  public function getCoursesPassed();
}
