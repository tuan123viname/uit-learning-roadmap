<?php

namespace Drupal\student\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;
use Drupal\user\Entity\User;
use mysql_xdevapi\Exception;

class ImportForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'student_import_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['file'] = [
      '#title' => $this->t('Student file'),
      '#type' => 'managed_file',
      '#description' => $this->t('Import your files'),
      '#upload_validators' => [
        'file_validate_extensions' => ['json'],
      ],
      '#upload_location' => 'public://student',
      '#multiple' => FALSE,
      '#required' => TRUE,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit')
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var File $file */
    $file = File::load($form_state->getValue(['file',0]));
    $file_content = file_get_contents($file->getFileUri());
    $students_data = json_decode($file_content);
    $operations = [];
    while (count($students_data) > 0) {
      $data = array_splice($students_data, 0, 50);
      $operations[] = [
        '\Drupal\student\Form\ImportForm::createStudents',
        [
          $data
        ]
      ];
    }
    $batch = [
      'title' => $this->t('Creating students'),
      'operations' => $operations,
      'finished' => '\Drupal\student\Form\ImportForm::finishCreateStudent'
    ];
    batch_set($batch);
  }

  /**
   * @param $data
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public static function createStudents($data) {
    foreach ($data as $student) {
      try {
        User::create([
          'field_full_name' => $student->name,
          'field_code' =>  $student->code,
          'mail' => $student->code . '@gm.uit.edu.vn',
          'name' => $student->code . '@gm.uit.edu.vn',
          'roles' => ['target_id' => 'student'],
          'status' => 1,
          'pass' => 123
        ])->save();
      }
      catch(Exception $exception) {
        \Drupal::logger('student')->notice("Create student fail");
      }
    }
  }

  /**
   * Log message when complete create student
   */
  public static function finishCreateStudent() {
    \Drupal::messenger()->addMessage('Create student successfully');
  }

}
