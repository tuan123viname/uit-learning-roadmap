<?php

namespace Drupal\student\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\TermInterface;
use Drupal\training_program\TrainingProgramManagerInterface;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Roadmap extends FormBase {

  /**
   * @var TrainingProgramManagerInterface
   */
  protected $trainingProgramManager;

  /**
   * @var AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Roadmap constructor.
   * @param AccountProxyInterface $account_proxy
   * @param TrainingProgramManagerInterface $training_program_manager
   */
  public function __construct(AccountProxyInterface $account_proxy,TrainingProgramManagerInterface $training_program_manager) {
    $this->currentUser = $account_proxy;
    $this->trainingProgramManager = $training_program_manager;
  }

  /**
   * @param ContainerInterface $container
   * @return Roadmap|static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user'),
      $container->get('training_program.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'roadmap_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $user = User::load($this->currentUser->id());

    $certificate = $this->getRequest()->get('certificate');
    $english = $this->getRequest()->get('english');
    $orientation = $this->getRequest()->get('orientation');
    $semester = $this->getRequest()->get('semester');
    $specialize_id = $this->getRequest()->get('specialize');


    if ($user && $user->get('field_training_program')->entity) {
      $training_program = $user->get('field_training_program')->entity;
      $form_state->set('training_program_id', $training_program->id());
      $major_id = $this->trainingProgramManager->getMajorId($training_program);
      /** @var TermInterface $specializes */
      $specializes = $this->trainingProgramManager->getSpecializesFromMajorId($major_id);
      $specialize_options = [];

      foreach ($specializes as $specialize) {
        $specialize_options[$specialize->id()] = $specialize->label();
    }

      $form['certificate'] = [
        '#title' => 'Đã có chứng chỉ tiếng anh',
        '#type' => 'checkbox',
        '#default_value' => $certificate
      ];
      $form['english'] = [
        '#title' => 'Tiếng anh đầu vào',
        '#type' => 'select',
        '#options' => [
          '1' => 'Anh văn 1',
          '2' => 'Anh văn 2',
          '3' => 'Anh văn 3',
          '4' => 'Anh văn 4',
          '5' => 'Anh văn 5',
        ],
        '#default_value' => $english,
        '#states' => [
          'invisible' => [
            ':input[name="certificate"]' => ['checked' => TRUE],
          ],
        ],
      ];
      $form['orientation'] = [
        '#type' => 'select',
        '#options' => [
          'thesis' => 'Luận văn',
          'replace' => '3 môn thay thế'
        ],
        '#title' => 'Định hướng',
        '#default_value' => $orientation
      ];
      $form['semester'] = [
        '#title' => 'Số kỳ',
        '#type' => 'select',
        '#options' => [
          '1' => 1,
          '2' => 2,
          '3' => 3,
          '4' => 4,
          '5' => 5,
          '6' => 6,
          '7' => 7,
          '8' => 8
        ],
        '#default_value' => $semester
      ];
      $form['specialize'] = [
        '#type' => 'select',
        '#options' => $specialize_options,
        '#title' => 'Chuyên ngành',
        '#default_value' => $specialize_id
      ];
      $form['submit'] = [
        '#type' => 'submit',
        '#value' => 'Submit'
      ];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $certificate = $form_state->getValue('certificate');
    $english = $form_state->getValue('english');
    $orientation = $form_state->getValue('orientation');
    $semester = $form_state->getValue('semester');
    $specialize = $form_state->getValue('specialize');
    $training_program_id = $form_state->get('training_program_id');
    $form_state->setRedirect('student.roadmap', [
      'certificate' => $certificate,
      'english' => $english,
      'orientation' => $orientation,
      'semester' => $semester,
      'specialize' => $specialize,
      'training_program_id' => $training_program_id
    ]);
  }

}
