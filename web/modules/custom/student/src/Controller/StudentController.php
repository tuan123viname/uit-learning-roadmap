<?php

namespace Drupal\student\Controller;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Controller\ControllerBase;
use Drupal\course\Entity\Course;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Drupal\training_program\RecommendManagerInterface;
use Drupal\training_program\TrainingProgramManagerInterface;
use Drupal\uit_class\ClassManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use GuzzleHttp\Client;

class StudentController extends ControllerBase {

  /**
   * @var ClassManager
   */
  protected $classManager;

  /**
   * @var RecommendManagerInterface
   */
  protected $recommendManager;

  /**
   * @var RequestStack
   */
  protected $requestStack;

  /**
   * @var \Symfony\Component\HttpFoundation\Request|null
   */
  protected $request;

  /**
   * @var TrainingProgramManagerInterface
   */
  protected $trainingProgramManager;

  /**
   * StudentController constructor.
   * @param RequestStack $request_stack
   */
  public function __construct(RequestStack $request_stack, TrainingProgramManagerInterface $training_program_manager, RecommendManagerInterface $recommend_manager, ClassManager $class_manager) {
    $this->requestStack = $request_stack;
    $this->request = $request_stack->getCurrentRequest();
    $this->trainingProgramManager = $training_program_manager;
    $this->recommendManager = $recommend_manager;
    $this->classManager = $class_manager;
  }

  /**
   * @param ContainerInterface $container
   * @return StudentController|static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('training_program.manager'),
      $container->get('recommend.manager'),
      $container->get('uit_class.class_manager')
    );
  }

  /**
   * Get roadmap for student
   *
   * @return array
   */
  public function roadmap() {
      $students = \Drupal::entityQuery('user')->condition('roles', ['target_id' => 'student'])->execute();
      $teachers = \Drupal::entityQuery('user')->condition('roles', ['target_id' => 'teacher'])->execute();
        $data = "";
      foreach ($teachers as $key=>$teacher){
          $data.=$teacher."-";
      }
      \Drupal::logger("concac")->error(print_r($data,TRUE));

    $table_score = $this->classManager->getScoreBoardByUserId(\Drupal::currentUser()->id());


    $road_map_form = $this->formBuilder()->getForm('\Drupal\student\Form\Roadmap');
    $build['form'] = $road_map_form;
    $certificate = $this->request->get('certificate');
    $english = $this->request->get('english');
    $orientation = $this->request->get('orientation');
    $semester = $this->request->get('semester');
    $specialize = $this->request->get('specialize');
    $training_program_id = $this->request->get('training_program_id');
    $training_program =  Node::load($training_program_id);
    if ($training_program_id) {
      /** @var NodeInterface $sample_roadmap */
      $sample_roadmap = $this->trainingProgramManager->getSampleRoadmap($training_program_id);
      $field_course_by_semesters = $sample_roadmap->get('field_course_by_semester')->referencedEntities();
      $course_by_semesters = [];
      foreach ($field_course_by_semesters as $course_by_semester) {
        $course_by_semesters[$course_by_semester->get('field_semester')->value] = $course_by_semester->get('field_training_program_course')->referencedEntities();
      }

      $this->recommendManager->setCoursesBySemester($course_by_semesters);
      $this->recommendManager->setTrainingProgramId($training_program_id);
      $course_by_semesters = $this->recommendManager->getRecommend([
        'english' => $english,
        'certificate' => $certificate,
        'specialize' => $specialize,
        'semester' => $semester,
        'orientation' => $orientation
      ]);

      if ($course_by_semesters == FALSE) {
        $this->messenger()->addWarning('Lộ trình không thẻ đáp ứng số kỳ mà bạn mong muốn bởi vì đã vượt quá số tín chỉ tối đa cho mỗi kỳ');
        return $road_map_form;
      }
//      $courses_passed = $this->recommendManager->getCoursesPassed();
//      dump($courses_passed);exit();
//      if (!empty($courses_passed)) {
//        array_unshift($course_by_semesters, ['courses_passed' => $courses_passed]);
//      }


      //Show list class passed
      $accumulate_credit = 0;
      $result = $this->entityTypeManager()->getStorage('node')->loadByProperties([
        'type' => 'student_class',
        'field_student' => \Drupal::currentUser()->id(),
        'field_status' => 'passed'
      ]);
      if (count($result) > 0) {
        $build['content']['title']['#markup'] = '<h4>' . 'Các lớp đã học' . '</h4>';
        $build['class_passed'] = [
          '#type' => 'table',
          '#header' => [
            'STT',
            'Tên lớp học',
            'Điểm số',
          ],
        ];
        $count = 0;

        foreach ($result as $item) {
          $accumulate_credit += $item->get('field_class')->entity->get('field_course')->entity->get('credit')->value;
          $count++;
          $build['class_passed']['#rows'][] = [
            $count,
            $item->get('field_class')->entity->label(),
            $item->get('field_score')->value,
          ];
        }
        //end show class list passed
      }

      $build['header']['title'] = [
        '#markup' => '<br/><br/><h3>Chương trình đào tạo ' . $training_program->label() . '</h3>'
      ];
      $build['header']['total_credit'] = [
        '#markup' => '<h4>Số tín chỉ yêu cầu: ' . $training_program->get('field_total_credit')->value . '</h4>'
      ];
     
      $build['header']['accumulate_credit'] = [
        '#markup' => '<h4>' . 'Số tín chỉ đã tích luỹ: '. $accumulate_credit . '</h4>'
      ];

      $build['roadmap'] = [
        '#type' => 'table',
        '#header' => [
          'semester' => 'Kỳ',
          'course' => 'Môn học',
          'credit' => 'Tín chỉ'
        ],
        '#rows' => []
      ];
        $baseUrl = 'http://localhost:9000';
        $dataRecommend = [];
        $client = new Client();
        try {
            \Drupal::logger('dmm')->error(print_r($this->currentUser()->id(),TRUE));

            $res = $client->request('POST', $baseUrl . '/student/recommend-subject/',[
                'body' => json_encode(['id_student' => $this->currentUser()->id()])
            ]);
            $dataRecommend = Json::decode($res->getBody());

            \Drupal::logger('pahon')->error(print_r($res->getBody(),TRUE));
        }
        catch (\Exception $exception) {
            \Drupal::logger('post_comment')->error('Get recommend Teacher failed ');
        }

        $notUse = false;
        if(count($dataRecommend['result']) < 4){
            $notUse=true;
        }
      $flag = TRUE;
        $totalCreditNow = 0;
        $tooFewCredit =false;
      $countMulTi=0;
      foreach ($course_by_semesters as $key => $semester) {
          $credit_by_semester = 0;
          if (count($semester) == 0) {
            break;
          }
          $build['roadmap']['#rows'][$key] = [
            'semester' => $key,
          ];
          $build_item = [
            '#theme' => 'course_list',
            '#courses' => [],
            '#has_header' => $flag
          ];
          $flag = FALSE;

          foreach ($semester as $index => $item) {
            /** @var Course $course */
            $course = $item->get('field_course')->entity;
            if ($item->get('field_course_type')->entity) {
              if($item->get('field_course_type')->entity->id()==17){
                if($notUse==false){
                  if($countMulTi <=3){
                    $course = Course::load($dataRecommend['result'][$countMulTi]);
                    $countMulTi++;
                  }
                }
              }
            }
              
            $course->setData('link', $course->toLink());
            $course->setData('course_type', $item->get('field_course_type')->entity);
            $credit_by_semester += $course->get('credit')->value;
            $build_item['#courses'][$index] = $course;
          }
          if($credit_by_semester< 14){
                $tooFewCredit = true;
          }
          $totalCreditNow +=$credit_by_semester;
          $build['roadmap']['#rows'][$key]['course']['data'] = $build_item;
          $build['roadmap']['#rows'][$key]['credit'] = $credit_by_semester;
      }
    }

    if($tooFewCredit){
//        $totalCC = $training_program->get('field_total_credit')->value;
//        $needLearn = $totalCC - $totalCreditNow;
//        $this->messenger()->addWarning('Bạn cần học thêm '.$needLearn.' trong khoảng'. round($totalCreditNow/14).' bạn sẽ bị cảnh cáo học vụ nếu học trong '.$semester);
//        return $road_map_form;
    }

    if(array_key_exists('students',$dataRecommend)){
        $dataStudents=[];
        foreach ($dataRecommend['students'] as $key=>$student){
            $dataStudents[] = ['student'=>$student,'scoreSi'=> 100- round($dataRecommend['diffs'][$key])];

        }
        $build['pahon_student']['course']['data']= [
            '#theme' => 'pahon_student',
            '#students' => $dataStudents
        ];
    }

    if (count($dataRecommend['result']) > 0) {
      $build['optional_course_title']['#markup'] = '<br /> <br /><h3>' . 'Các môn học tự chọn khác' . '</h3>';
      $build['optional_courses'] = [
        '#type' => 'table',
        '#header' => [
          'STT',
          'Tên môn học',
          'Số tín chỉ'
        ],
      ];
      $count_optional = 1;

      foreach ($dataRecommend['result'] as $key => $item) {
        if ($key >= 4) {

          $build['optional_courses']['#rows'][] = [
            $count_optional,
            Course::load($item)->link(),
            Course::load($item)->get('credit')->value,
          ];
          $count_optional++;
        }
      }
    }


    return $build;
  }


  public function myClass($user) {

    $build = [
      '#type' => 'table',
      '#header' => [
        'STT',
        'Tên lớp học',
        'Điểm số',
        'Trạng thái'
      ],
    ];

    if($user->id() == \Drupal::currentUser()->id()){
        $build['#title'] = 'Lớp học của tôi';
    }else{
        $build['#title'] = 'Lộ trình có năng lực giống bạn';
    }

    $result = $this->entityTypeManager()->getStorage('node')->loadByProperties([
      'type' => 'student_class',
      'field_student' => $user->id(),
    ]);
    $count = 0;
    foreach ($result as $item) {
      $count++;
      $build['#rows'][] = [
        $count,
        $item->get('field_class')->entity->label(),
        $item->get('field_score')->value,
        $item->get('field_status')->value
      ];
    }

    return $build;
  }
}
