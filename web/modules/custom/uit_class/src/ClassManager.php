<?php

namespace Drupal\uit_class;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\node\Entity\Node;

class ClassManager {

  /**
   * @var EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * ClassManager constructor.
   * @param EntityTypeManagerInterface $entity_type_manager
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Get  student information list in class
   */
  public function getStudentsInfoClass($class_id, $limit = 50) {
    $results = $this->entityTypeManager->getStorage('node')->getQuery()
      ->condition('type', 'student_class')
      ->condition('field_class', $class_id)
      ->pager($limit)
      ->execute();
    return Node::loadMultiple($results);
  }

  /**
   * @param $uid
   */
  public function getScoreBoardByUserId($uid) {
    $table_score = [];
    $result = $this->entityTypeManager->getStorage('node')->loadByProperties([
      'type' => 'student_class',
      'field_student' => $uid
    ]);
    foreach ($result as $item) {
      $table_score[$item->get('field_class')->entity->get('field_course')->entity->id()] = $item->get('field_score')->value;
    }
    return $table_score;
  }

}