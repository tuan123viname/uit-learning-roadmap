<?php

namespace Drupal\uit_class\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;
use GuzzleHttp\Client;

class GenerateStudentClass extends FormBase {

  /**
   * {@inheritdoc }
   */
  public function getFormId() {
    return 'generate_student_class';
  }

  /**
   * {@inheritdoc }
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['class_ids'] = [
      '#type' => 'textfield',
      '#title' => 'Class IDs',
    ];
    $form['student_ids'] = [
      '#type' => 'textarea',
      '#title' => 'Student IDs',
      '#size' => 1000
    ];
      $form['type'] = [
          '#title' => 'Loại môn',
          '#type' => 'select',
          '#options' => [
              'TC' => 'Tự chọn',
              'DC' => 'Đại cương',
              'CSN' => 'Cơ sở ngành',
              'CN' => 'Chuyên ngành',
              'CDTN' => 'Chuyên đề tốt nghiệp',
              'KLTN' => 'Khóa luận',
          ]
      ];
    $form['submit']  = [
      '#type' => 'submit',
      '#value' => 'Submit'
    ];
    return $form;
  }

  private function callAPIUpdate($idStudent, $idCourse,$score){
      $baseUrl = 'http://localhost:9000';
      $client = new Client();
      $res = $client->request('POST', $baseUrl . '/student/create-sample-roadmap/',[
          'body' => json_encode([
              'id_student' => $idStudent,
              'id_course' => $idCourse,
              'score' => $score
          ])
      ]);
  }

  private function deleteData($idStudents,$idCourses){
      foreach ($idStudents as $idStudent){
          foreach ($idCourses as $idCourse){
              $result = \Drupal::entityTypeManager()->getStorage('node')->loadByProperties([
                  'type' => 'student_class',
                  'field_student' => $idStudent
              ]);

              foreach ($result as $item) {
                  if(in_array($item->get('field_class')->entity->get('field_course')->entity->id(),$idCourses)){
                      \Drupal::logger("asddasdasd")->error("deleted");

                      $item->delete();
                  }

              }
          }
      }
  }


  private function updateSpecifyCourse($idStudents,$course,$min,$max){
      foreach ($idStudents as $student){
          $table_score = [];
          $result = \Drupal::entityTypeManager()->getStorage('node')->loadByProperties([
              'type' => 'student_class',
              'field_student' => $student,
              'field_course'=>$course
          ]);

          foreach ($result as $item) {
              $score = rand($min, $max) + rand(0, 10) / 10;
              if($score>10){
                  $score=10;
              }
              $item->field_score->setValue($score);
              $item->field_status->setValue($score > 5 ? 'passed' : 'failed');
              $item->save();
              $this->callAPIUpdate($student,$item->get('field_class')->entity->get('field_course')->entity->id(),$score);
          }
      }
    }

    private function updateScore($idStudents,$min,$max){
      foreach ($idStudents as $student){
          $table_score = [];
          $result = \Drupal::entityTypeManager()->getStorage('node')->loadByProperties([
              'type' => 'student_class',
              'field_student' => $student
          ]);

          foreach ($result as $item) {
              $score = rand($min, $max) + rand(0, 10) / 10;
              if($score>10){
                    $score=10;
                }
              $item->field_score->setValue($score);
              $item->field_status->setValue($score > 5 ? 'passed' : 'failed');
              $item->save();
              $this->callAPIUpdate($student,$item->get('field_class')->entity->get('field_course')->entity->id(),$score);
          }
      }
    }

  /**
   * {@inheritdoc }
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
      
    $class_ids = json_decode($form_state->getValue('class_ids'));
    $student_ids = json_decode($form_state->getValue('student_ids'));
    $type = $form_state->getValue('type');

    foreach ($class_ids as $class_id) {
      foreach ($student_ids as $student_id) {

        $score = rand(0, 10) + rand(0, 10) / 10;
        if($score>10){
            $score=10;
        }
        Node::create([
          'title' => User::load($student_id)->get('field_full_name')->value,
          'type' => 'student_class',
          'field_class' => $class_id,
          'field_student' => $student_id,
          'field_score' => $score,
          'field_status' => $score > 5 ? 'passed' : 'failed',
            'field_type_subject'=>$type
        ])->save();
          \Drupal::logger('pahon_type2')->warning(print_r($type,TRUE));

      }
    }


  }
}