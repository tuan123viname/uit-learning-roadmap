<?php

namespace Drupal\uit_class\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;

class AddStudent extends FormBase {

  public function getFormId() {
    return 'uit_class_add_student';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['student']= [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Sinh viên'),
      '#target_type' => 'user',
      '#tags' => TRUE,
      '#required' => TRUE
    ];
    $form['status'] = [
      '#type' => 'select',
      '#title' => 'Trạng thái',
      '#options' => [
        'passed' => 'Passed',
        'studying' => 'Studying',
        'failed' => 'Failed'
      ]
    ];
    $form['score'] = [
      '#type' => 'number',
      '#title' => 'Điểm số',
      '#default_value' => 0
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Thêm sinh viên'
    ];
    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $nid = $this->getRequest()->get('nid');
    $student_id = $form_state->getValue('student')[0]['target_id'];
    Node::create([
      'type' => 'student_class',
      'title' => $student_id,
      'field_status' => $form_state->getValue('status'),
      'field_score' => $form_state->getValue('score'),
      'field_class' => $nid,
      'field_student' => $student_id
    ])->save();
    $this->messenger()->addMessage('Thêm sinh viên vào lớp thành công');
    $form_state->setRedirect('uit_class.detail', [
      'node' => $nid
    ]);
  }

}