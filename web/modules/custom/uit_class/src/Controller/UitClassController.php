<?php

namespace Drupal\uit_class\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\NodeInterface;
use Drupal\uit_class\ClassManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class UitClassController extends ControllerBase {

  /**
   * @var ClassManager
   */
  protected $classManager;

  /**
   * UitClassController constructor.
   * @param ClassManager $class_manager
   */
  public function __construct(ClassManager $class_manager) {
    $this->classManager = $class_manager;
  }

  /**
   * @param ContainerInterface $container
   * @return UitClassController|static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('uit_class.class_manager')
    );
  }

  /**
   * @return string[]
   */
  public function detailClass(NodeInterface $node) {
    $students_info = $this->classManager->getStudentsInfoClass($node->id(), 50);
    $build = [];
    $build['content'] = [
      '#theme' => 'detail_class',
      '#class' => $node,
      '#data' => $students_info
    ];
    $build['pager'] = [
      '#type' => 'pager'
    ];
    return $build;
  }

}