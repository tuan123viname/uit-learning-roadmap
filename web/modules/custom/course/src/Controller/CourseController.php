<?php

namespace Drupal\course\Controller;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Controller\ControllerBase;
use Drupal\course\Entity\Course;
use GuzzleHttp\Client;

class CourseController extends ControllerBase {

  /**
   * Show detail course information
   *
   * @param Course $course
   * @return string[]
   */
  public function courseDetail(Course $course) {

    $baseUrl = 'http://localhost:9000';
    $data = NULL;
    $client = new Client();
    try {
      $res = $client->request('POST', $baseUrl . '/teacher/recommend-teacher/',[
        'body' => json_encode(['id_student' => $this->currentUser()->id()])
      ]);
      $data = Json::decode($res->getBody());
    }
    catch (\Exception $exception) {
      \Drupal::logger('post_comment')->error('Get recommend Teacher failed ');
    }

    $documents = $course->get('field_documents')->referencedEntities();
    $docs = [];
    foreach ($documents as $item) {
      $docs[] = [
        'label' => $item->label(),
        'url' => $item->url()
      ];

    }


      $teacherRecommended = [];
    foreach ($course->get('field_teachers')->referencedEntities() as $teacher){
        if(in_array($teacher->id(),$data)){
            array_push($teacherRecommended,["name"=>$teacher.field_full_name.value,"id"=>$teacher->id()]);
        }
    }

      foreach ($course->get('field_teachers')->referencedEntities() as $teacher){
          if(!in_array($teacher->id(),$data)){
              array_push($teacherRecommended,["name"=>$teacher->field_full_name->value,"id"=>$teacher->id()]);
          }
      }

    return [
      '#theme' => 'course_detail',
      '#course' => $course,
      '#teachers' => $teacherRecommended,
      '#documents' => $docs
    ];
  }

}
