<?php

namespace Drupal\course\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\course\Entity\Course;
use Drupal\file\Entity\File;

class ImportForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'course_import_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['file'] = [
      '#title' => $this->t('Course file'),
      '#type' => 'managed_file',
      '#description' => $this->t('Import your files'),
      '#upload_validators' => [
        'file_validate_extensions' => ['json'],
      ],
      '#upload_location' => 'public://course',
      '#multiple' => FALSE,
      '#required' => TRUE,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit')
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var File $file */
    $file = File::load($form_state->getValue(['file',0]));
    $file_content = file_get_contents($file->getFileUri());
    $courses_data = json_decode($file_content);
    $operations = [];
    while (count($courses_data) > 0) {
      $data = array_splice($courses_data, 0, 50);
      $operations[] = [
        '\Drupal\course\Form\ImportForm::createCourses',
        [
          $data
        ]
      ];
    }
    $batch = [
      'title' => $this->t('Creating courses'),
      'operations' => $operations,
      'finished' => '\Drupal\course\Form\ImportForm::finishCreateCourse'
    ];
    batch_set($batch);
  }

  /**
   * @param $data
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public static function createCourses($data) {

    foreach ($data as $course) {
      $course_ids= \Drupal::entityQuery('course')->condition('name', $course->name)->execute();
      if (empty($course_ids)) {
        Course::create([
          'name' => $course->name,
          'goal' =>  $course->goal,
          'description' =>  $course->description,
          'type' => $course->type,
          'code' => $course->code,
          'category' => $course->category,
          'practice_credit' => $course->practiceCredit,
          'theoretical_credit' => $course->theoreticalCredit,
          'credit' => $course->credit
        ])->save();
      }
    }
  }

  /**
   * Log message when complete create courses
   */
  public static function finishCreateCourse() {
    \Drupal::messenger()->addMessage('Create courses successfully');
  }
}
