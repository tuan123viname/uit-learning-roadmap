<?php

namespace Drupal\course\Entity;

use Drupal\Core\Config\Entity\ConfigEntityDependency;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EditorialContentEntityBase;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\user\EntityOwnerInterface;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the Course entity.
 *
 * @ContentEntityType(
 *   id = "course",
 *   label = @Translation("Course"),
 *   handlers = {
 *     "views_data"   = "Drupal\views\EntityViewsData",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\course\CourseListBuilder",
 *     "form" = {
 *       "default" = "Drupal\course\Form\CourseForm",
 *       "add" = "Drupal\course\Form\CourseForm",
 *       "edit" = "Drupal\course\Form\CourseForm",
 *       "delete" = "Drupal\course\Form\CourseDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\Core\Entity\Form\DeleteMultipleForm",
 *     },
 *     "access" = "Drupal\entity\EntityAccessControlHandler",
 *     "permission_provider" = "Drupal\entity\EntityPermissionProvider",
 *     "local_task_provider" = {
 *       "default" = "Drupal\entity\Menu\DefaultEntityLocalTaskProvider",
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "course",
 *   data_table = "course_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer course entity",
 *   entity_keys = {
 *     "id" = "course_id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "collection" = "/admin/uit-tools/courses",
 *     "canonical" = "/course/{course}",
 *     "add-form" = "/admin/uit-tools/course/add",
 *     "edit-form" = "/admin/uit-tools/course/{course}/edit",
 *     "delete-form" = "/admin/uit-tools/course/{course}/delete",
 *     "delete-multiple-form" = "/admin/uit-tools/course/delete",
 *   },
 *   field_ui_base_route = "entity.course.collection",
 * )
 *
 */

class Course extends EditorialContentEntityBase implements ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  use EntityOwnerTrait;

  protected $data;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += self::ownerBaseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setRequired(TRUE)
      ->setDisplayOptions('view',[
        'type' => 'string',
        'weight' => 1
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 1
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['goal'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Goal'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 3000)
      ->setDisplayOptions('view',[
        'type' => 'string',
        'weight' => 2
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 2
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['description'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Description'))
      ->setSetting('max_length', 3000)
      ->setRequired(TRUE)
      ->setDisplayOptions('view',[
        'type' => 'string',
        'weight' => 3
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 3
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['category'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Category'))
      ->setSetting('target_type', 'taxonomy_term')
      ->setRequired(TRUE)
      ->setSetting('handler_settings',['target_bundles' => ['category']])
      ->setDisplayOptions('view',[
        'type' => 'string',
        'weight' => 4
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 4
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['credit'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Credit'))
      ->setRequired(TRUE)
      ->setDisplayOptions('view',[
        'type' => 'string',
        'weight' => 5
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 5
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Type'))
      ->setRequired(TRUE)
      ->setDisplayOptions('view',[
        'type' => 'string',
        'weight' => 1
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 1
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);
    $fields['code'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Code'))
      ->setRequired(TRUE)
      ->setDisplayOptions('view',[
        'type' => 'string',
        'weight' => 1
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 1
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['theoretical_credit'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Theoretical credit'))
      ->setRequired(TRUE)
      ->setDisplayOptions('view',[
        'type' => 'string',
        'weight' => 1
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 1
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['practice_credit'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Practice credit'))
      ->setRequired(TRUE)
      ->setDisplayOptions('view',[
        'type' => 'string',
        'weight' => 1
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 1
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created');
    $fields['changed'] = BaseFieldDefinition::create('changed');
    return $fields;
  }

  /**
   * @param $key
   * @param $value
   * @return $this
   */
  public function setData($key, $value) {
    $this->data[$key] = $value;
    return $this;
  }

  /**
   * @param $key
   * @return mixed
   */
  public function getData($key) {
    return $this->data[$key];
  }

}
