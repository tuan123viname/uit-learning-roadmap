<?php

namespace Drupal\uit_comment;

use Drupal\Component\Serialization\Json;
use GuzzleHttp\Client;

class CommentManager implements CommentManagerInterface {

  protected $baseUrl = 'http://localhost:9000';

  /**
   * {@inheritdoc}
   */
  public function postComment($post_data) {
    $client = new Client();
    try {
      $res = $client->request('POST', $this->baseUrl . '/teacher/create-comment/',[
        'body' => json_encode($post_data)
      ]);
      $data = Json::decode($res->getBody());
      return $data;
    }
    catch (\Exception $exception) {
      \Drupal::logger('post_comment')->error('Post comment failed');
    }

  }

}
