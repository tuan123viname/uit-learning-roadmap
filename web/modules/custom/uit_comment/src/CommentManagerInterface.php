<?php

namespace Drupal\uit_comment;

interface CommentManagerInterface {

  /**
   * Post a comment to python server
   *
   * @param $comment
   * @return mixed
   */
  public function postComment($comment);

}
