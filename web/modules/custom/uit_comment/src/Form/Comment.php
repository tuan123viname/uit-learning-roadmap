<?php

namespace Drupal\uit_comment\Form;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;
use Drupal\uit_comment\CommentManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

define('comment',[]);

class Comment extends FormBase {

  /**
   * @var CommentManagerInterface
   */
  protected $commentManager;

  /**
   * Comment constructor.
   * @param CommentManagerInterface $comment_manager
   */
  public function __construct(CommentManagerInterface $comment_manager) {
    $this->commentManager = $comment_manager;
  }

  /**
   * @param ContainerInterface $container
   * @return Comment|static
   */
  public static function create(ContainerInterface $container) {
      $students = \Drupal::entityQuery('user')->condition('roles', ['target_id' => 'student'])->execute();
      $teachers = \Drupal::entityQuery('user')->condition('roles', ['target_id' => 'teacher'])->execute();

      \Drupal::logger("concac")->error(print_r($students,TRUE));
    return new static($container->get('uit_comment.comment_manager'));
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'comment_form_id';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $entity = $this->getRequest()->get('course');

    if (!$entity) {
      $entity = $this->getRequest()->get('user');
    }
    if ($entity) {
      $form_state->set('entity', $entity);
    }
    $form['#atributes']['class'] = 'form-comment-container';
    $form['subject'] = [
      '#type' => 'textfield',
      '#title' => 'Subject',
      '#default_value' => 'Ý kiến đóng góp',
      '#required' => TRUE,
    ];
    $form['content'] = [
      '#type' => 'textarea',
      '#title' => 'Content',
      '#required' => TRUE
    ];
    $form['submit']  = [
      '#type' => 'submit',
      '#value' => 'Comment',
    ];
    $form['#cache'] = ['max-age' => 0];
    return $form;
  }

  public function postCommentOk($teacher,$student,$content,$entity){
      $post_data = [
          'uid' => (int)$student,
          'content_id' => (int)$teacher,
          'commend' => $content,
          'type' => 'user'
      ];
      $data = $this->commentManager->postComment($post_data);
      /** @var EntityInterface $entity */

      if ($entity->id()) {
          Node::create([
              'title' => 'Ý kiến đóng góp',
              'type' => 'comment',
              'field_content' => $content,
              'field_target_bundle' => $entity->bundle(),
              'field_target_entity_id' => $teacher,
              'field_target_entity_type' => 'user',
              'field_good' => json_decode($data)->result
          ])->save();
      }
  }

  public function generateCommend($entity){
      $students = \Drupal::entityQuery('user')->condition('roles', ['target_id' => 'student'])->execute();
      $teachers = \Drupal::entityQuery('user')->condition('roles', ['target_id' => 'teacher'])->execute();

      \Drupal::logger("concac")->error(print_r($students,TRUE));

      $numNeedRandom = 88;

      foreach ($students as $student){
          $radded =[];
          for($i=0;$i<88;$i++){
              $index = rand(284,397);
              while(in_array($index,$radded) && !array_key_exists($index,$teachers)){
                  $index = rand(284,397);
              }
              array_push($radded,$index);
              $this->postCommentOk($index,$student,comment[rand(0,count(comment)-1)],$entity);
          }
      }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
      $students = \Drupal::entityQuery('user')->condition('roles', ['target_id' => 'student'])->execute();
      $teachers = \Drupal::entityQuery('user')->condition('roles', ['target_id' => 'teacher'])->execute();

//      \Drupal::logger("concac")->error(print_r($students,TRUE));
////      die();
    $entity = $form_state->get('entity');
    $content = $form_state->getValue('content');
    $post_data = [
      'uid' => \Drupal::currentUser()->id(),
      'content_id' => $entity->id(),
      'commend' => $content,
      'type' => $entity->bundle()
    ];
    $data = $this->commentManager->postComment($post_data);
    /** @var EntityInterface $entity */

    if ($entity->id()) {
      Node::create([
        'title' => $form_state->getValue('subject'),
        'type' => 'comment',
        'field_content' => $content,
        'field_target_bundle' => $entity->bundle(),
        'field_target_entity_id' => $entity->id(),
        'field_target_entity_type' => $entity->getEntityTypeId(),
        'field_good' => json_decode($data)->result
      ])->save();
      \Drupal::messenger()->addMessage('Hệ thống ghi nhận ý kiến đóng góp của bạn');
    }
  }

}
