<?php

namespace Drupal\uit_comment\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Comment form' Block.
 *
 * @Block(
 *   id = "comment_form_block",
 *   admin_label = @Translation("Comment form block"),
 *   category = @Translation("Comment form"),
 * )
 */

class CommentForm extends BlockBase {

  public function build() {
    $comment_form = \Drupal::formBuilder()->getForm('Drupal\uit_comment\Form\Comment');
    return $comment_form;
  }

}
