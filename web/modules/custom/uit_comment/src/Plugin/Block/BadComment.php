<?php

namespace Drupal\uit_comment\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\node\Entity\Node;

/**
 * Provides a 'Bad comment list' Block.
 *
 * @Block(
 *   id = "bad_comment_list",
 *   admin_label = @Translation("Bad comments"),
 *   category = @Translation("Comment"),
 * )
 */

class BadComment extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    /** @var EntityInterface $entity */
    $entity = \Drupal::request()->get('course');

    if (!$entity) {
      $entity = \Drupal::request()->get('user');
    }
    if ($entity) {

      $results = \Drupal::entityQuery('node')
        ->condition('type', 'comment')
        ->condition('field_target_bundle', $entity->bundle())
        ->condition('field_target_entity_id', $entity->id())
        ->condition('field_target_entity_type', $entity->getEntityTypeId())
        ->condition('field_good', 0)
        ->pager(10)
        ->execute();
      $comments = Node::loadMultiple($results);
     
      $build['comments'] = [
        '#theme' => 'bad_comments',
        '#comments' => $comments,
        '#cache' =>['max-age' => 0]
      ];
      $build['pager'] = [
        '#type' => 'pager',
      ];
    }

    return $build;
  }

}