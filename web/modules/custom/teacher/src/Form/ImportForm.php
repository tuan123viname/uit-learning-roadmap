<?php

namespace Drupal\teacher\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\course\Entity\Course;
use Drupal\file\Entity\File;
use Drupal\user\Entity\User;

class ImportForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'teacher_import_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['file'] = [
      '#title' => $this->t('Teacher file'),
      '#type' => 'managed_file',
      '#description' => $this->t('Import your files'),
      '#upload_validators' => [
        'file_validate_extensions' => ['json'],
      ],
      '#upload_location' => 'public://teacher',
      '#multiple' => FALSE,
      '#required' => TRUE,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit')
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var File $file */
    $file = File::load($form_state->getValue(['file',0]));
    $file_content = file_get_contents($file->getFileUri());
    $teachers_data = json_decode($file_content);
    $operations = [];
    while (count($teachers_data) > 0) {
      $data = array_splice($teachers_data, 0, 50);
      $operations[] = [
        '\Drupal\teacher\Form\ImportForm::createTeachers',
        [
          $data
        ]
      ];
    }
    $batch = [
      'title' => $this->t('Creating teachers'),
      'operations' => $operations,
      'finished' => '\Drupal\teacher\Form\ImportForm::finishCreateTeacher'
    ];
    batch_set($batch);
  }

  /**
   * @param $data
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public static function createTeachers($data) {
    foreach ($data as $teacher) {
      try {
        User::create([
          'field_full_name' => $teacher->name,
          'mail' => $teacher->email,
          'name' => $teacher->email,
          'roles' => ['target_id' => 'teacher'],
          'status' => 1,
          'pass' => 123
        ])->save();
      }
      catch(\Exception $exception) {
        \Drupal::logger('teacher')->notice("Create teacher fail");
      }
    }
  }

  /**
   * Log message when complete create student
   */
  public static function finishCreateTeacher() {

    \Drupal::messenger()->addMessage('Create teacher successfully');
  }

}
